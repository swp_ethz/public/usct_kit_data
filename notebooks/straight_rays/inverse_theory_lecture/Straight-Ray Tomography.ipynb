{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "34f5f875",
   "metadata": {},
   "source": [
    "# Straight-ray tomography\n",
    "\n",
    "In the following notebook, we perform a simple straight-ray tomography in 2D. This serves to illustrate some of the basic concept of linear least-squares inversion, such as prior and posterior covariances, damping, smoothing, over- and under-fitting, and resolution."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a5c69076",
   "metadata": {},
   "source": [
    "# 0. Import some Python packages\n",
    "\n",
    "We begin by importing some Python packages for matrix-vector operations, for plotting, and for computing the forward modelling matrix $\\mathbf{G}$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e36cbf7b",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Some Python packages.\n",
    "\n",
    "import numpy as np\n",
    "import scipy\n",
    "from scipy.sparse import linalg\n",
    "\n",
    "import sys\n",
    "\n",
    "sys.path.insert(0, \"./utils\")  # This contains functions to compute G.\n",
    "from grid import *\n",
    "from straight_ray_tracer import *\n",
    "\n",
    "# Set some parameters to make plots nicer.\n",
    "\n",
    "plt.rcParams[\"font.family\"] = \"serif\"\n",
    "plt.rcParams.update({\"font.size\": 35})"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "64fa55d4",
   "metadata": {},
   "source": [
    "# 1. Basic input\n",
    "\n",
    "In the following few lines, we define the most basic geometric input, including the dimensions of the model domain, as well as the positions of sources and receivers. The initial setup mimics a cross-hole tomography where sources are on one side of the domain, and receivers are on the other one."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "49dfe0a8",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define the numerical grid. ---------------------------------------------\n",
    "dimension = 2  # Here we only consider 2D problems anyway.\n",
    "x_min = 0.0  # Minimum x-coordinate\n",
    "y_min = 0.0  # Minimum y-coordinate\n",
    "dx = 2.5  # Grid spacing in x-direction\n",
    "dy = 2.5  # Grid spacing in y-direction\n",
    "Nx = 20.0  # Number of grid points in x-direction\n",
    "Ny = 20.0  # Number of grid points in y-direction\n",
    "g = grid(dimension, [x_min, y_min], [dx, dy], np.array([Nx, Ny]))\n",
    "\n",
    "# Sources and receivers. -------------------------------------------------\n",
    "src_locations = np.array([0.0 * np.ones((11,)), np.linspace(0, 50, 11)])\n",
    "rec_locations = np.array([50.0 * np.ones((21,)), np.linspace(0, 50, 21)])\n",
    "\n",
    "sources, receivers = get_all_to_all_locations(src_locations, rec_locations)\n",
    "plot_rays(sources, receivers, g)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8dbb0fa8",
   "metadata": {},
   "source": [
    "# 2. Compute forward matrix G\n",
    "\n",
    "Knowing source and receiver positions, and the setup of the domain, we can compute the forward modelling matrix **G** that connects a slowness model **m** to a synthetic data vector **d** via **d**=**Gm**. In addition to computing **G**, we also visualise the ray density and the entries of **G**. We will see that ray density is rather uneven, and that **G** is pretty sparse."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1a323fd3",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Compute G and measure how long that takes.\n",
    "G = create_forward_operator(sources, receivers, g)\n",
    "\n",
    "# Print some statistics of G.\n",
    "print(\"Matrix shape:            \", G.shape)\n",
    "print(\"Data points:             \", G.shape[0])\n",
    "print(\"Unknowns in model space: \", G.shape[1])\n",
    "print(\"Non-zero entries:        \", G.count_nonzero())\n",
    "print(\n",
    "    \"Ratio of non-zeros: {:10.4f} %\".format(\n",
    "        100 * G.count_nonzero() / (G.shape[0] * G.shape[1])\n",
    "    )\n",
    ")\n",
    "\n",
    "# Plot ray density and entries of G.\n",
    "plot_ray_density(G, g)\n",
    "\n",
    "# Plot non-zero matrix entries.\n",
    "print(\"Sparsity pattern of the forward matrix:\")\n",
    "plt.figure(figsize=(15, 20))\n",
    "plt.spy(G, markersize=2, color=\"k\")\n",
    "plt.gca().xaxis.tick_bottom()\n",
    "plt.xlabel(\"model space index\")\n",
    "plt.ylabel(\"data space index\")\n",
    "plt.title(r\"non-zero entries of $\\mathbf{G}$\")\n",
    "plt.savefig(\"non-zeros.pdf\", format=\"pdf\")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6ae8290b",
   "metadata": {},
   "source": [
    "# 3. Create target model\n",
    "\n",
    "Since we work with artifical and not with real traveltime data, we need to define a target model that we wish to reconstruct. In the basic setup, our target model is a simple chequerboard pattern of slowness values."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6bcd4a1b",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Input model setup (chequerboard). --------------------------------------\n",
    "dvp = 100.0  # velcity variations in m/s.\n",
    "dd = 4  # Width of the chequerboard cells in number of cells.\n",
    "\n",
    "# Allocate velocity matrix. Homogeneous background model.\n",
    "vp = 3000.0 * np.ones(g.npoints)\n",
    "\n",
    "# Add some heterogeneities\n",
    "s = 1.0\n",
    "for i in range(0, g.npoints[0], dd):\n",
    "\n",
    "    for j in range(0, g.npoints[1], dd):\n",
    "        end_i = min(g.npoints[0], i + dd)\n",
    "        end_j = min(g.npoints[1], j + dd)\n",
    "        vp[i:end_i, j:end_j] += s * dvp\n",
    "        s *= -1\n",
    "\n",
    "m_true = (1 / vp).ravel()\n",
    "\n",
    "clim = [1 / 3.1, 1 / 2.9]\n",
    "plot_model(\n",
    "    1000.0 * m_true, g, \"true model [ms/m]\", caxis=clim, savename=\"true_model.pdf\"\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b8da7e4f",
   "metadata": {},
   "source": [
    "# 4. Generate synthetic measurements\n",
    "\n",
    "Having defined the target model, it remains to compute artificial traveltime data that enter the observed data vector $\\mathbf{d}^\\text{obs}$. To be more realistic, we add random errors to the traveltime data. The amplitude of these errors are defined by the parameter *sigma_d*. Knowing *sigma_d*, we then compute the prior data covariance $\\mathbf{C}_D$.\n",
    "\n",
    "In the real world, measurement errors will usually not do us the favour of being nicely normally distributed. Even if they were, it could be difficult to estimate the correct prior data covariance."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "59919e21",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create observed data ---------------------------------------------------\n",
    "d_true = G * m_true\n",
    "\n",
    "# Prior covariance parameters. -------------------------------------------\n",
    "sigma_d = 0.2e-4  # Data standard deviation.\n",
    "d_obs = d_true + sigma_d * np.random.randn(len(d_true))\n",
    "\n",
    "# Data covariance matrix. ------------------------------------------------\n",
    "Cd = sigma_d ** 2 * scipy.sparse.eye(len(d_obs))\n",
    "Cd_inv = 1 / sigma_d ** 2 * scipy.sparse.eye(len(d_obs))\n",
    "\n",
    "# Traveltimes.\n",
    "plt.subplots(figsize=(15, 10))\n",
    "plt.plot(1000.0 * d_obs, \"k\")\n",
    "plt.ylabel(\"travel time [ms]\")\n",
    "plt.xlabel(\"ray path idx\")\n",
    "plt.show()\n",
    "\n",
    "# Traveltime errors.\n",
    "plt.subplots(figsize=(15, 10))\n",
    "plt.plot(1000.0 * (d_obs - d_true), \"k\")\n",
    "plt.ylabel(\"travel time errors [ms]\")\n",
    "plt.xlabel(\"ray path idx\")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d8ed8877",
   "metadata": {},
   "source": [
    "# 5. Compute prior model covariance\n",
    "\n",
    "The final step before solving the inverse problem is to define the prior mean model $\\mathbf{m}^\\text{prior}$, and the prior covariance in model space, $\\mathbf{C}_M$. For the latter, we need to define the correlation length, contained in the parameter *corr_len*. To visualise the effect of $\\mathbf{C}_M$, we can plot its action on the target model."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "46fa3e48",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Prior model. -----------------------------------------------------------\n",
    "m_prior = np.ones(m_true.shape) / 3000.0\n",
    "\n",
    "# Prior covariance. ------------------------------------------------------\n",
    "correlation_length = 3.0  # lambda\n",
    "regularization_weight = 2.5e-5  # sigma_M\n",
    "\n",
    "Cm = g.get_gaussian_prior(correlation_length)\n",
    "\n",
    "plot_model(1000.0 * m_prior, g, \"prior model [ms/m]\", caxis=clim)\n",
    "plot_model(1000.0 * Cm * m_true, g, \"smoothed true model [ms/m]\", caxis=clim)\n",
    "\n",
    "Cm *= regularization_weight ** 2\n",
    "Cm_inv = linalg.inv(Cm)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "347bf7a2",
   "metadata": {},
   "source": [
    "# 6. Solve inverse problem\n",
    "\n",
    "We are now equipped with all ingredients needed to solve the inverse problem. For this, we need to compute the inverse of the Hessian of the least-squares misfit functional, $\\mathbf{C}_M^{-1}+\\mathbf{G}^T \\mathbf{C}_D^{-1} \\mathbf{G}$, which is equal to the posterior covariance, $\\mathbf{\\tilde{C}}_M$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5293e5ec",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Hessian ----------------------------------------------------------------\n",
    "H = G.T * Cd_inv * G + Cm_inv\n",
    "\n",
    "# Posterior covariance ---------------------------------------------------\n",
    "Cm_post = scipy.sparse.linalg.inv(H)\n",
    "\n",
    "# Posterior mean. --------------------------------------------------------\n",
    "m_est = Cm_post * (G.T * Cd_inv * d_obs + Cm_inv * m_prior)\n",
    "d_est = G * m_est\n",
    "d_prior = G * m_prior\n",
    "\n",
    "# Plot. ------------------------------------------------------------------\n",
    "plot_model(\n",
    "    1000.0 * m_est,\n",
    "    g,\n",
    "    \"reconstructed slowness [ms/m]\",\n",
    "    caxis=clim,\n",
    "    savename=\"reconstructed_model.pdf\",\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "288da0bc",
   "metadata": {},
   "source": [
    "# 7. Data fit and posterior covariance\n",
    "\n",
    "To check for over- or under-fitting, we compute the rms misfit. For an assessment of model quality, we visualise the prior and posterior covariances."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "43ab8620",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Compute estimated data.\n",
    "d_est = G * m_est\n",
    "\n",
    "# Compute and print rms misfit.\n",
    "N = len(d_est)\n",
    "rms = np.sqrt(np.sum((d_est - d_obs) ** 2) / (N * sigma_d ** 2))\n",
    "print(\"rms misfit %f\" % rms)\n",
    "\n",
    "# Plot traveltime residuals.\n",
    "plt.figure(figsize=(15, 8))\n",
    "plt.plot(1000.0 * (d_obs - d_est), \"kx\")\n",
    "plt.plot([0.0, N], [1000.0 * sigma_d, 1000.0 * sigma_d], \"--k\")\n",
    "plt.plot([0.0, N], [-1000.0 * sigma_d, -1000.0 * sigma_d], \"--k\")\n",
    "plt.title(\"traveltime residuals [ms]\", pad=20)\n",
    "plt.xlim([0.0, N])\n",
    "plt.savefig(\"residuals.pdf\", format=\"pdf\")\n",
    "plt.show()\n",
    "\n",
    "# Plot prior and posterior covariances.\n",
    "Cmpd = Cm_post.todense()\n",
    "Cmd = Cm.todense()\n",
    "\n",
    "x = np.zeros(len(m_est))\n",
    "xp = np.zeros(len(m_est))\n",
    "\n",
    "for i in range(len(x)):\n",
    "    x[i] = 1.0e9 * Cm[210, i]\n",
    "    xp[i] = 1.0e9 * Cmpd[210, i]\n",
    "\n",
    "plt.figure(figsize=(15, 8))\n",
    "plt.plot(x, \"--k\")\n",
    "plt.plot(xp, \"k\")\n",
    "plt.xlim([150, 250])\n",
    "plt.grid()\n",
    "plt.title(\"covariance [ms**2/s**2]\", pad=20)\n",
    "plt.savefig(\"covariance.pdf\", format=\"pdf\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e6ff8180",
   "metadata": {},
   "source": [
    "# 8. Model resolution\n",
    "\n",
    "To formally assess the resolution of our model, we compute the resolution matrix and visualise some point-spread functions and averaging kernels."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c37f152b",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Compute generalised inverse.\n",
    "Ginv = Cm * G.T * scipy.sparse.linalg.inv(Cd + G * Cm * G.T)\n",
    "# Compute model resolution matrix.\n",
    "R = Ginv * G\n",
    "\n",
    "# Plot model resolution matrix.\n",
    "R = R.todense()\n",
    "plt.figure(figsize=(25, 25))\n",
    "plt.imshow(R, vmin=-0.6, vmax=0.6, cmap=\"Greys\")\n",
    "plt.colorbar(shrink=0.75)\n",
    "plt.title(\"model resolution matrix\", pad=20)\n",
    "plt.tight_layout()\n",
    "plt.savefig(\"resolution.pdf\", format=\"pdf\")\n",
    "plt.show()\n",
    "\n",
    "# Number of resolved parameters.\n",
    "print(\"number of resolved parametrers: %f\" % np.trace(R))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e0c2886d",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Extract and plot averaging kernels (rows of the resolution matrix).\n",
    "row = 30\n",
    "avk = R[row, :].reshape(int(Nx), int(Ny))\n",
    "\n",
    "clim = [-0.25, 0.25]\n",
    "plot_model(avk, g, \"averaging kernel\", caxis=clim, savename=\"avk.pdf\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7cf9c770",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Extract and plot point-spread functions (columns of the resolution matrix).\n",
    "column = 315\n",
    "psf = R[:, column].reshape(int(Nx), int(Ny))\n",
    "\n",
    "clim = [-0.25, 0.25]\n",
    "plot_model(psf, g, \"point-spread function\", caxis=clim, savename=\"psf.pdf\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "503bc840",
   "metadata": {},
   "source": [
    "# 9. Data resolution"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9fb0e07f",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Compute generalised inverse.\n",
    "Ginv = Cm * G.T * scipy.sparse.linalg.inv(Cd + G * Cm * G.T)\n",
    "# Compute model resolution matrix.\n",
    "Rd = G * Ginv\n",
    "\n",
    "# Plot data resolution matrix.\n",
    "Rd = Rd.todense()\n",
    "plt.figure(figsize=(25, 25))\n",
    "plt.imshow(Rd, vmin=-0.6, vmax=0.6, cmap=\"Greys\")\n",
    "plt.colorbar(shrink=0.75)\n",
    "plt.title(\"data resolution matrix\", pad=20)\n",
    "plt.tight_layout()\n",
    "plt.savefig(\"resolution_data.pdf\", format=\"pdf\")\n",
    "plt.show()\n",
    "\n",
    "# Plot diagonal of data resolution matrix.\n",
    "plt.figure(figsize=(15, 10))\n",
    "plt.plot(np.diag(Rd), \"k\")\n",
    "plt.title(\"diagonal of data resolution matrix\")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1e4b2ccb",
   "metadata": {},
   "source": [
    "# 10. Singular value analysis and nullspace"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b0dcb819",
   "metadata": {
    "lines_to_next_cell": 2
   },
   "outputs": [],
   "source": [
    "# Singular-value decomposition\n",
    "# u,s,vt=scipy.sparse.linalg.svds(G,k=len(d_obs)-1)\n",
    "u, s, vt = scipy.sparse.linalg.svds(G.T * G, k=399)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6f5d19ee",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot singular vectors.\n",
    "k = 2\n",
    "scale = np.max(np.abs(vt[k, :]))\n",
    "clim = [-scale, scale]\n",
    "plot_model(vt[k, :], g, \"eigenmodel\", caxis=clim, savename=\"eigenmodel.pdf\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "57680452",
   "metadata": {},
   "source": [
    "# Exercises\n",
    "\n",
    "**Exercise 1**: Plot the difference between (artificial) observed data and data estimated from the posterior mean model. Are the observations explained to within their uncertainties? Are the data over- or under-fit?\n",
    "\n",
    "**Exercise 2**: Plot the posterior covariance for a model parameter near the centre and a model parameter near the edge of the domain. How do the posterior variances and covariances compare? Is the result in accord with your intuitive expectations based on the ray coverage plot?\n",
    "\n",
    "**Exercise 3**: Compute and plot an L-curve for the smoothing parameter (correlation length). Choose an optimal smoothing based on the L-curve.\n",
    "\n",
    "**Exercise 4**: Compute and visualise the resolution matix."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
