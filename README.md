 # USCT_KIT_data

This repo holds a collection of codes useful when working with medical ultrasound data. There are examples for classical reflection/B-mode imaging as well as straight-ray and bent-ray tomography algorithms. This collection is aimed to provide an easy introduction to common techniques used when working with medical ultrasound data. The notebooks presented here are specifically targeted for data collected with ultrasound computed tomography (USCT) devices. Data used in the notebooks is openly accesilble here:
[usct_data](https://usct.gitlab.io/datachallenge2019/data/).
To run and install all prerequisites, please create a new environment by running
```
conda env create -f usct.yml -n usct
```
using the provided `usct.yml`. Note: this requires a working conda environment which can be installed from here [Anaconda](https://www.anaconda.com/) or here [Miniconda](https://docs.conda.io/en/latest/miniconda.html). Then, activate the environement with
```
conda activate usct
```
which should enable you to run the notebooks. If you encounter any problems, please contact <ines.ulrich@erdw.ethz.ch>.
 
